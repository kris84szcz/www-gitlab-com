---
layout: markdown_page
title: "Product Vision - Monitor"
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product vision for Monitor. If you'd like to discuss this vision
directly with the product manager for Monitor, feel free to reach out to Sarah Waldner via [e-mail](mailto:swaldner@gitlab.com) or by [scheduling a video call](https://calendly.com/swaldner-gitlab/30min).

## Overview

The Monitor stage comes after you've configured your production infrastructure and
deployed your application to it. As part of the verification and release process
you've done some performance validation - but you need to ensure your service(s) maintain
the expected service-level objectives ([SLO](https://en.wikipedia.org/wiki/Service-level_objective)s)
for your users. 

GitLab's Monitor stage product offering makes instrumentation of your service easy,
giving you the right tools to prevent, respond to, and restore SLO degradation. Current DevOps teams either
lack exposure to operational tools or utilize ones that put them in a reactive position when complex systems
fail inexplicably. Our mission is to empower your DevOps teams by finding operational issues before they hit production and enabling them to respond like pros by leveraging default SLOs and responses they proactively instrumented. GitLab Monitoring allows you to successfully complete the DevOps loop, not just for the features in your product,
but for its performance and user experience as well. 

Using dashboards, and in the future a status page, we provide an easy way for you to gain a holistic understanding of the state of your production services across multiple groups and projects. When you are deploying a suite of services, it's critical that you can drill into each individual services SLO attainment as well as troubleshoot issues which span multiple services.

We track epics for all the major deliverables associated with the north stars, and
category maturity levels. You can view them on our [Monitor Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=✓&state=opened&label_name[]=devops%3A%3Amonitor).

## North Stars

We're pursuing a few key principles within the Monitor Stage.

### Instrument with ease

Your team's service(s), first and foremost, need to be observable before you are able to evaluate production performance characteristics. We believe that observability should be easy. GitLab will ship
with smart conventions that setup your applications with generic observability. We will also
make it simple to instrument your service, so that custom metrics, ones that you'd like to build your own SLOs around, can be added with a few lines of code. 

Related Categories:
* Metrics
* Logging
* Error Tracking
* Synthetic Monitoring
* Cluster Monitoring

Related Epics:
* [Service Level Objectives and Agreements](https://gitlab.com/groups/gitlab-org/-/epics/432)


### Resolve like a pro

We want to help teams resolve outages faster, accelerating both the
troubleshooting and resolution of incidents. GitLab's single platform can
correlate the incoming observability data with known CI/CD events and source code
information, to automatically suggest potential root causes.

Related Categories:
* Incident Management
  
Related Epics:
* [Incident Management: Viable Maturity Plan](https://gitlab.com/groups/gitlab-org/-/epics/1493)
* [Correlate Deployments to Incidents](https://gitlab.com/groups/gitlab-org/-/epics/420)
* [Correlate Feature Flags to Incidents](https://gitlab.com/groups/gitlab-org/-/epics/421)
* [Correlate Errors to Incidents](https://gitlab.com/groups/gitlab-org/-/epics/419)
* [Incident Specific Triage Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/1435)

### Gain insights seamlessly

Continuously learning and driving those insights back into your development cycle is a critical part
of the DevOps loop. The tools in the Monitor stage make it possible to gain insights about
production SLOs, incidents and observability sources across the multi-project systems that make
up a complete application. 

Container based deployments have rapidly expanded the amount of observability information available. 
It is no longer possible to collate and visualize this information without automation and distillation of valuable insights which GitLab can do for you.

We'll also provide views across a suite of applications so that managers of a large number of DevOps or Operations teams can get a quick view of their application suite, and team's health. 

Related Categories:
* Status Page
* Incident Management
* Tracing

## Principles

Our north stars are the guide posts for where we are headed. Our principles inform how we will
get there. First and foremost we abide by GitLab's universal [Product Principles](https://about.gitlab.com/handbook/product/#product-principles).
There are a few unique principles to the Monitor stage itself. 

### Complete the Loop First

As part of our general principle of [Flow One](/handbook/product/#flow-one) the Monitor stage will
seek to complete the full observability feedback loop for limited use cases first, before moving on
to support others. As a starting point this will mean supoprt for [modern](/handbook/product/#modern-first),
[cloud-native](/handbook/product/#cloud-native-first) [developers](/handbook/product/#developers-first) 
first.

### Observability for those who operate

In modern DevOps organizations developers are expected to also operate the services they develop. In
many cases this expectation isn't met. Whether a developer is the one operating an application or not, we will build tools that work for those doing the operator job. This means forgoing preferences, like developers to avoid deep production troubleshooting, and instead building tools that allow those who operate to be best-in-class operators, regardless of their title. 

### Dogfooding

Our users can't expect a complete set of Monitoring tools if we don't utilize it ourselves for
instrumenting and operating GitLab. That's why we will [dogfood everything](/handbook/product/#dogfood-everything).

We will start with GitLab Self-Monitoring and our own Infrastructure teams. We want self-managed administrator users
to utilize the same tools to observe and respond to health alerts about their GitLab instance as they would to
monitor their own services. We'll also complete our own DevOps loop by having our Infrastructure teams for GitLab.com
utilize our incident management feature.

<%= partial("direction/categories", :locals => { :stageKey => "monitor" }) %>

## Prioritization Process

In general, we follow the same [prioritization guidelines](/handbook/product/#prioritization)
as the product team at large. Issues will tend to flow from having no milestone,
to being added to the backlog, to being added to this page and/or a specific
milestone for delivery.

You can see our entire public backlog for Monitor at this
[link](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Monitoring);
filtering by labels or milestones will allow you to explore. If you find
something you're interested in, you're encouraged to jump into the conversation
and participate. At GitLab, everyone can contribute!

Issues with the "direction" label have been flagged as being particularly
interesting, and are listed in the section below.

## Upcoming Releases

<%= direction %>

<%= partial("direction/other", :locals => { :stage => "monitor" }) %>

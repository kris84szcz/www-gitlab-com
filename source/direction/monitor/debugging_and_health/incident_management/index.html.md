---
layout: markdown_page
title: "Category Vision - Incident Management"
---

- TOC
{:toc}

## Incident Management
Incident Management is a collection of features which enable organizations to effectively manage outages and other events that occur while operating a service.

In its most simplest form, it is a single source of truth (SSOT) for understanding:

* The current state of an incident 
* The communication channels where an incident is being worked (Zoom, Slack, etc.)
* Relevant information to an incident
  * Code related: commits, merge requests, code, releases
  * Operations related: alerts, errors, metrics, traces, logs, runbooks, deployments 

### Incidents in GitLab

We plan to leverage GitLab's existing Issue features as a base for Incident Management, and extend them with additional features. This provides a few benefits:

* Smaller MVC and iterations, by re-using existing features
* Faster time to market
* Reduced incremental cognitive load, as we are re-using existing workflows and concepts
* Improvements to existing features can benefit a wider set of use cases beyond Incident Management

#### High level design

* An Incident is a GitLab Issue.
* [Key services can be embedded directly in Issues](https://gitlab.com/gitlab-org/gitlab-ce/issues/55757)
  * Zoom calls, Slack channels, Pagerduty, Metrics, Errors, and more
  * For example, a Zoom call for the incident could be started by simply having `/zoom` in the description/template
* Description templates can then be used to establish a common set of tools, format, and notifications
  * Zoom calls, Pagerduty pages,  and other actions can be taken/embedded automatically
* Rich notification controls already exists for Issues
  * @ mentioning users/groups, posting to a Slack channel, user preferences, etc.
  * [We can enhance to better fit our use cases](https://gitlab.com/gitlab-org/gitlab-ce/issues/55828)
* [Comment pinning to better understand significant updates](https://gitlab.com/groups/gitlab-org/-/epics/630)
* [Alerts will automatically open Incidents based on the template](https://gitlab.com/gitlab-org/gitlab-ce/issues/55814)
* [Attribution of incident to Error Budgets can also be performed](https://gitlab.com/gitlab-org/gitlab-ee/issues/8232)

#### Out of scope

These features are currently out of scope for Incident Management:

* Paging and on-call schedules
* Escalation
* Remediation

## What's Next & Why
We are just starting to build out features specific for Incident Management.

[The MVC is next](https://gitlab.com/groups/gitlab-org/-/epics/662)

## Maturity Plan
* [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/349)
  * [MVC Epic](https://gitlab.com/groups/gitlab-org/-/epics/662)

## Competitive Landscape
* [Atlassian OpsGenie](https://www.opsgenie.com/)
* [Splunk VictorOps](https://victorops.com/)
* [PagerDuty](https://www.pagerduty.com/)

## Analyst Landscape
Not yet, but accepting merge requests to this document.

## Top Customer Success/Sales Issue(s)
Not yet, but accepting merge requests to this document.

## Top Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Internal Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Vision Item(s)
Not yet, but accepting merge requests to this document.

---
layout: markdown_page
title: "Companies that got inspired by GitLab's culture and/or GitLab's Handbook"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Which companies can be listed here?

Any company that got inspired by GitLab's culture and/or by any part of our handbook can be listed here.

[Transparency](/handbook/values/#transparency) is one of [GitLab's values](/handbook/values), and so we encourage you and your company to read our handbook and use the parts that make sense to your business, or even adapt it to your context.

With that, we want to know what on GitLab's culture or on GitLab's handbook inspired you, and how are you and your company using it to the benefit your business?

To be listed in this page, [open an issue](https://gitlab.com/gitlab-com/www-gitlab-com/issues) with the following information:

**Title:** Add company XYZ on got inspired handbook page / Update company XYX on got inspired handbook page

**Description:** Tells us what exactly was the inspiration, with a link to the handbook page so that others can have a look, and maybe get inspired as well. Optionally, send us your company's website URL so that we can link to it.

**Label:** handbook

Or, if you feel comfortable with making the change yourself, submit a [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests), and we will be happy to review and have it merged as soon as possible. Don't forget to add the label 'handbook'.

### List of companies that got inspired by GitLab

In this section we list the companies that got inspired by GitLab with the links to the handbook pages that inspired them.

- Placeholder for company name (optionally clickable and linking to the company's website)
  - Placeholder for what inspired the company with a link to the handbook (add as many items as you want)

## Do you know of other companies that were inspired by GitLab but are not listed here?

Let us know by [opening an issue](https://gitlab.com/gitlab-com/www-gitlab-com/issues) to include them in this page, with the company's name and their website so that we can contact them and list their experience here as well, to inspire others. Don't forget to add the label 'handbook'.

---
layout: markdown_page
title: "SYS.1.06 - Log Reconciliation: CMDB Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SYS.1.06 - Log Reconciliation: CMDB

## Control Statement

GitLab reconciles the established device inventory against the enterprise log repository quarterly; devices which do not forward log data are remediated.

## Context

This control is a partner control to CON.1.04 (Configuration Check Reconciliation: CMDB). The purpose of this control is to validate that all devices in the device inventory have corresponding logs. This control is simply a validation of both logging configurations and the GitLab device inventory.

## Scope

This control applies to all production systems in the device inventory.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SYS.1.06_log_reconciliation_cmdb.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SYS.1.06_log_reconciliation_cmdb.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SYS.1.06_log_reconciliation_cmdb.md).

## Framework Mapping

* ISO
  * A.12.4.1
* SOC2 CC
  * CC1.2
  * CC3.2
  * CC3.4
  * CC4.1
  * CC4.2
  * CC5.1
  * CC5.2

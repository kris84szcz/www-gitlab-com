---
layout: markdown_page
title: "VUL.6.01 - External Information Security Inquiries Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# VUL.6.01 - External Information Security Inquiries

## Control Statement

GitLab reviews information-security-related inquiries, complaints, and disputes.

## Context

The purpose of this control is to have some way for GitLab to engage with stakeholders (e.g. customers, contributors, security researchers, etc.) around information security topics. This control is foundational to a lot of our compliance responsibilities including GDPR since stakeholders need to have a way to reach us with questions or concerns about their personal information.

## Scope

This control applies to all GitLab systems and stakeholders.

## Ownership

The GitLab security team handles these inquiries, complaints, and disputes.

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/VUL.6.01_external_information_security_inquiries.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/VUL.6.01_external_information_security_inquiries.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/VUL.6.01_external_information_security_inquiries.md).

## Framework Mapping

* SOC2 CC
  * CC7.1

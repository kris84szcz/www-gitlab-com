---
layout: markdown_page
title: "Quality Department"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Child Pages

#### [On-boarding](/handbook/engineering/quality/onboarding)
#### [Guidelines](/handbook/engineering/quality/guidelines)
##### [Test Engineering](/handbook/engineering/quality/guidelines/test-engineering)
##### [Triage Operations](/handbook/engineering/quality/guidelines/triage-operations)
#### [Project Management](/handbook/engineering/quality/project-management)
#### [Roadmap](/handbook/engineering/quality/roadmap)

## Vision

Ensure that GitLab consistently releases high-quality software across the growing suite of products, developing software at scale without sacrificing quality, stability or velocity.
The quality of the product is our collective responsibility. The quality department makes sure everyone is aware of what the quality of the product is, empirically.

To execute on this, we categorize our vision into the following areas.

### Culture of Quality

#### Champion an early culture of quality in the development process
{:.no_toc}
* Review product requirements and designs to identify risk and steer the team on quality strategy.
* Review code in collaboration with counterpart teams to ensure adequate test coverage is present.
* Partner with all development teams to ensure that testability is built in.
* Be a champion for better software design, promote proper testing practices and bug prevention strategies.

#### Timely feedback loop
{:.no_toc}
* Use the data from test results to improve test gaps in our test suites.
* Conduct test gap analysis from bugs that are filed by GitLab users.
* Be a sounding-board for our end users and non-engineering stakeholders by integrating their feedback on quality into the development process.

### Test Coverage

#### Test framework reliability and efficiency
{:.no_toc}
* Expand end-to-end testing framework [GitLab QA].
* Maintain our Continuous Integration & Delivery test pipelines.
* Improve on the duration and de-duplication of the GitLab test suites.
* Improve stability by eliminating flaky tests and transient failures.

#### Improve test coverage
{:.no_toc}
* Groom the [test pyramid](https://martinfowler.com/bliki/TestPyramid.html) coverage and ensure that the right tests run at the right place.
* Improve functional performance test coverage.
* Improve third-party service integration test coverage.
* Improve test results tracking and reporting mechanisms that aid in triaging test results.
* Coach developers (internal & external) on contributing to [GitLab QA] test scenarios.

#### Self-managed usecase
{:.no_toc}
* Build test infrastructure and environments for self-managed customer usecases.
* Capture self-managed customer usage and improve test coverage for customers' GitLab instances.

#### Improve Quality in release process
{:.no_toc}
* Eradicate obstacles in the release process.
* Ensure that we have a repeatable and consistent Quality process for every release.

### Engineering Productivity
* Make metrics-driven suggestions to improve engineering processes, velocity, and throughput.
* Build productivity tooling to help speed up development efforts.
* Build automated tools to ensure the consistency and quality of the codebase and merge request workflow.

## Quality Engineering Teams

##### [Dev QE team](dev-qe-team)
##### [Ops & CI/CD QE team](ops-qe-team)
##### [Secure & Enablement QE team](secure-enablement-qe-team)
##### [Engineering Productivity team](engineering-productivity-team)

### Department members

<%= stable_counterparts(role_regexp: /(Test Automation|Engineering Productivity|Quality Engineering)/, direct_manager_role: 'Director of Quality Engineering') %>


## Stable counterparts

Every Test Automation Engineer is aligned with a Product Manager and is responsible for the same features their Product Manager oversees.
They work alongside Product Managers and engineering at each stage of the process: planning, implementation, testing and further iterations.
The area a Test Automation Engineer is responsible for is part of their title; for example, "Test Automation Engineer, Plan." as defined in the [team org chart](/company/team/org-chart/).

Every Quality Engineering Manger is aligned with an Engineering Director in the Development Department.
They work at a higher level and align cross-team efforts which maps to a [Development Department section](/handbook/product/categories/#hierarchy).
The area a Quality Engineering Manger is responsible for is part of their title; for example, "Quality Engineering Manager, Dev" as defined in the [team org chart](/company/team/org-chart/).
This is with the exception of the Engineering Productivity team which is based on the [span of control](/handbook/leadership/#management-group).

Full-stack Engineering Productivity Engineers develop features both internal and external which improves the efficiency of engineers and development processes.
Their work is separate from the regular release kickoff features per [areas of responsibility](/handbook/engineering/quality/engineering-productivity-team#areas-of-responsibility).

### Headcount planning

We plan headcount as follows:

* **One Test Automation Engineer for each group**
  * 1:1 ratio of Test Automation Engineer to [Product Groups](handbook/product/categories/#hierarchy).
  * Approximately a 1:10 ratio of Test Automation Engineer to Development Department Engineers.
  * Approximately 1:1 ratio of Test Automation Engineer to Product Managers.
* **One Quality Engineering Manager for each section**
  * 1:1 ratio of Quality Engineering Manager to [Development Department Sections](handbook/product/categories/#hierarchy).
  * Approximately a 1:1 ratio of Quality Engineering Manager to Development Department Directors.
* **One Engineering Productivity Engineer for each section**
  * Approximately a 1:1 ratio of Engineering Productivity Engineer to [Development Department Sections](handbook/product/categories/#hierarchy).
  * Approximately a 1:37 ratio of Engineering Productivity Engineers to Development Department Engineers.

## Failure Management Rotation

Every member in the Quality team shares the responsibility of analysing the daily QA tests against `master` and `staging` branches.
More details can be seen [here](/handbook/engineering/quality/guidelines/failure-management-rotation)

## Team retrospective

The Quality team holds a once-a-month asynchronous retrospective.
The process is automated and notes are captured in [Quality retrospectives](https://gitlab.com/gl-retrospectives/quality/) (GITLAB ONLY)

## Release process overview

Moved to [release documentation](https://gitlab.com/gitlab-org/release/docs/).

## Test Automation Framework

The GitLab test automation framework is distributed across three projects:

* [GitLab QA], the test orchestration tool.
* The scenarios and spec files within the GitLab codebase under `/qa` in both GitLab [CE] and [EE].

### Architecture overview

See the [GitLab QA Documentation](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs)
and [current architecture overview](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs/architecture.md).

### Installation and execution

* Install and set up the [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit)
* Install and run [GitLab QA] to kick off test execution.
  * The spec files (test cases) can be found in the [GitLab CE/EE codebase](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/qa)

### AMA sessions

Every quarter the Quality team will host an AMA session about the testing framework. The idea is to keep everyone informed about what's new and to answer questions.

The next sessions are scheduled for 2019/09/20, 2019/12/20, and 2020/03/20.

> Note: the dates mentioned above can change, but we will try to keep this document updated.

## Other related pages

* [Issue Triage Policies](/handbook/engineering/issue-triage)
* [Performance of GitLab](/handbook/engineering/performance)
* [Monitoring of GitLab.com](/handbook/engineering/monitoring)
* [Production Readiness Guide](https://gitlab.com/gitlab-com/infrastructure/blob/master/.gitlab/issue_templates/production_readiness.md)

[GitLab QA]: https://gitlab.com/gitlab-org/gitlab-qa
[GitLab Insights]: https://gitlab.com/gitlab-org/gitlab-insights
[GitLab Triage]: https://gitlab.com/gitlab-org/gitlab-triage
[CE]: https://gitlab.com/gitlab-org/gitlab-ce
[EE]: https://gitlab.com/gitlab-org/gitlab-ee

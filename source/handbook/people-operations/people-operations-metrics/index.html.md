---
layout: markdown_page
title: "People Group Metrics"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Reporting

The People Group pulls metrics reports each month, quarter, and year, as well as routinely for the People Team [Group Conversation](https://about.gitlab.com/handbook/people-operations/group-conversations/).

The monthly metrics reports can be found in the [Google Drive](https://drive.google.com/drive/u/1/folders/1UNisqJAJQbYiEplNKG0FsgKQkx4-qoHA) and is only accessible to those who contribute to and review the reports, as they contain confidential information about team members and candidates. The reports contain several tabs: "Summary", "Recruiting", "SAT", "Diversity", "Low Location Factor", and "Turnover". The  team is responsible for the the Low Location Factor and Turnover tabs, as well as summarizing their findings on the Summary tab. The reports are done in the month following the month that we are analyzing so that we are able to have the full picture (e.g. January's metrics report is done in February).


## Average Location Factor

The average location factor of all team members per department or division. The location factor directly correlates to geographical area in the [Compensation Calculator](/handbook/people-operations/global-compensation/#compensation-calculator). The company wide average location factor target is < 0.65. Each division and department also has their own average location factor target.

|   Division  |   Target  |
| ----------- | --------- |
| Alliances   |   0.82    |
| Development |   0.58    |
| G&A         |   0.69    |
| Marketing   |   0.72    |
| Product     |   0.72    |
| Sales       |   0.72    |
|**Grand Total** |  **0.65**   |

|   Department  |   Target  |
| ----------- | --------- |
| Business Development   |   0.82    |
| Business Operations |   0.70    |
| CEO         |   1.00    |
| Channel   |   0.86    |
| Commercial Sales     |   0.69    |
| Corporate Marketing       |   0.79    |
| Customer Solutions       |   0.74    |
| Customer Success       |   0.69    |
| Customer Support       |   0.61    |
| Demand Generation       |   0.67    |
| Development      |   0.54    |
| Digital Marketing       |   0.80    |
| Enterprise Sales       |   0.74    |
| Field Marketing       |   0.71    |
| Field Operations      |   0.89    |
| Finance       |   0.73    |
| Infrastructure       |   0.58    |
| Marketing Ops       |   0.71    |
| Meltano       |   0.67    |
| Outreach      |   0.66    |
| People Group      |   0.72    |
| Product Management     |   0.72    |
| Product Marketing       |   0.79    |
| Quality      |   0.58    |
| Recruiting      |   0.62    |
| Security      |   0.66    |
| UX      |   0.63    |
|**Grand Total** |  **0.65**   |

## Low Location Factor Reporting

1. Create the "Low Location Factor Factor Data" tab.
1. In columns A through E add the following information from the "Comp Data Analysis & Modeling" Google spreadsheet.
  * Employee ID
  * First Name
  * Last Name
  * City
  * Location Factor
  * In Range? (Metrics)
1. In columns H and on export and past the "Low Location Factor" Report from BambooHR. Remove anyone who was hired after the last day of the reporting month and ensure any terminated team members that were active as of the last day of the month are accounted for.
1. Run an audit to match Employee IDs and Cities from the comp calc to BambooHR and correct any misaligned information
1. Copy any new hires for the reporting month into the rows above as we will report on just that month's metrics separately.
1. Next, update the rolling three month's tab by adding this month's new hires and removing the new hires from what would be four months ago.
1. Generate pivot tables on a new tab totaling the average location factor for each department and division for the reporting month, rolling last three months, and company wide average location factors.
1. Update and audit the figures on the "Location Factor Graphs/Summary" tab which will automatically update the graph.
1. Copy and paste the graph to the summary page outlining anything of importance based on the analytics.
1. Update the slide deck with this information as needed.

## Percent Over Compensation Band

During the Location Factor reporting, People Operations Analysts will also analyze against how many team members in a division or department are compensated above the bands specific by our [Global Compensation](/handbook/people-operations/global-compensation/#compensation-principles) policy. To determine this, use the "In Range? (Metrics)" column from the Low Location Factor Reporting and generate a pivot table using a count of "FALSE" per department and division. Add this information to the "Location Factor Graphs/Summary" tab to generate a percentage based on total headcount per department and division as well as the raw number. The number can help explain the percentage if a department or division is small, for example.

The percent over compensation band target is = 0%.

TODO: Weight the percentage based on how far out of range a team member is. The Compensation and Benefits Manager will synch with the Finance Business Partner to present to the CFO.
Note: The percentages may fluctuate based on the results of our Compensation Review through Radford. If we change the compensation bands, we are also adjusting whether someone falls in or out of range.

## Onboarding SAT

New team member feedback of the onboarding experience in a given month. The Onboarding SAT target is > 4.

## Turnover

There are two parts to the turnover reporting: Turnover and Tenure.

1. Open the "Turnover Rates Workbook" google sheet.
1. Update the 2019 Term Data tab by running the "Additions and Terminations" Report in BambooHR for the reporting month. Populate the following columns, Time in Job, MOS Category,	Type,	Tenure in days. Carry down the average tenure and rolling 12 months tenure.
1. Update the Rolling Turnover tab.
  * Delete what would be the 13th month and add a new item in for the reporting month under rolling tenure and rolling turnover by month.
  * Pull the Turnover Report from BambooHR for the last rolling twelve months and update the information in the spreadsheet.
1. Update the 2019 Average Tenure Data Report
  * Pull the Average Tenure Data report from BambooHR for all active team members. Ensure anyone hired after the reporting month is removed and any terminated team members that were active at the end of the reporting month are included.
  * Add in the end date, and carry down the formula for tenure in days and the transfer to tenure in months/years.
  * Populate the summary box and transfer that data to the Rolling Turnover tab.
1. Copy and paste the table from the Rolling Turnover tab to the Turnover tab on the Metrics Reporting spreadsheet and the GitLab Turnover Rates workbook (viewable to the company)
1. Update the information on the Turnover tab: Voluntary/Involuntary term information (tag the HRBPs to update reasons), Tenure, and term percentages which update the graphs.
1. Add this information and a copy of the graphs to the summary tab and the slide deck as needed.

## Team Members

For calculating KPIs we define Team Members on the date measured as the number of full time equivalent employees or contractors who are providing services to GitLab and are listed on our Team page.
Excluded from this category are board members, board observers, core team members, and advisors.
The canonical source of truth of the number of team members comes from BambooHR.

## Team Member Turnover

Number of Team Members terminated/Average Total Team Member Count for the period measured.

Voluntary turnover is any instance in which a team member actively chooses to leave GitLab. GitLab measures voluntary turnover over a rolling 12 month period, as well as over a calendar month.  (The default period is over a rolling 12 month period.) The rolling 12 month voluntary team member turnover target is < 10%. In order to achieve the rolling 12 month voluntary team member turnover target, the monthly voluntary team member turnover target is < 0.83% (10/12). The data is housed in BambooHR.

Rolling Voluntary Team Member Turnover = (Number of Team Members actively choosing to leave GitLab/Average Total Team Members Count) x 100

Total Turnover = Voluntary Turnover + Involuntary Turnover

GitLab measures team members turnover over a rolling 12 month period, as well as over a calendar month. (The default period is over a rolling 12 month period.) The 12 month team member turnover target is < 16%.   In order to achieve the rolling 12 month team member turnover target, the monthly team member turnover target is < 1.3% (16/12). The data is housed in BambooHR.

## Cost Per Team Member

The cost per team member metric is intended to track variances across the company in compensation, discretionary bonuses, promotions, and involuntary attrition. This metric does not have an associated goal as the purpose is not to reduce costs, but instead understand the early indicators of something going wrong or what may be going well. Consistency should be the key evaluator of the KPI.

TODO (Comp/Ben): Develop a reporting mechanism in BambooHR to track variances of: 
1. compensation
1. discretionary bonuses
1. promotions
1. involuntary attrition
Once a report is generated that can be replicated or automated, add instructions to this page.

---
layout: job_family_page
title: "Solutions Manager"
---

The Solutions Manager is a product manager for the services offerings of GitLab. In partnership with the VP of Customer Success and other key stakeholders, the Solutions Manager is responsible for driving GitLab adoption and premier customer experiences through the development and delivery of world-class services and training solutions that help GitLab customers achieve their desired outcomes. 

## Responsibilities

- Define, scope, and prioritize GitLab’s services and training offerings, including persona and market segmentation analysis, offering definition, scope, and pricing
- Collaborate with subject matter experts in the field, Product Management & Marketing, and instructional designers (as appropriate) to develop and deliver prioritized global services and training offerings, content, and sales & marketing collateral
- Ensure services/training delivery model is focused on customer success outcomes while championing quality and efficiency
- Measure and report on the effectiveness of services and training enablement investments and programs
- Ensure a robust closed feedback loop that embraces continuous improvement and iteration
- Identify and act on opportunities to improve the customer experience via innovative services/training offerings
- Build business case for additional services and training and enablement resources as needed

<a id="intermediate-requirements"></a>
## Requirements

- Knowledge and familiarity with the Software Development Life Cycle and DevOps required (open source software knowledge and familiarity considered a plus)
- 3+ years relevant experience defining, developing, and executing customer services and/or training strategies, operations and action plans
- Ability to quickly understand technical concepts and explain them to customer and professional services audiences (mostly technical)
- Proven ability to effectively interact with and influence senior executives and team members
- Exceptional written/verbal communication and presentation skills
- Team player with strong interpersonal skills, skilled at project management and cross-functional collaboration
- Experienced in giving and receiving constructive feedback
- Ability to thrive in a fast-paced, unpredictable environment
- Share and work in accordance with GitLab's values
- Experience growing within a small start-up is a plus

## Specialties

### Professional Services & Consulting

#### Responsibilities

- Serve in a player/coach role balancing field delivery with operations and strategy
- Establish strategic plans and operational practices (e.g., people, skill sets, engagement approaches, processes and policies, enablement and coaching) for existing and new services
- Develop collateral to clearly describe and communicate the customer value of the service offerings (i.e., scope, deliverables, pricing)
- Successfully introduce and incubate new services to deliver on key success measures, including customer-facing engagements and internal initiatives
- Partner with the sales teams to build services forecasts for new and existing services
- Help to manage resource assignments and staffing levels, including recruitment as needed
- Identify and implement process and tooling improvements to help project managers and consultants better support external customer professional services requirements
- Coach, motivate and train professional service engineer team members and consultants to improve their delivery quality and efficiency
- Work with the Product Development and Support teams to contribute documentation for GitLab


### Training & Certification

#### Responsibilities

- Construct a short, medium, and long-term vision for free and fee-based customer and partner training and enablement offerings for successful deployment and implementation of GitLab solutions
- Define, scope, and prioritize training and enablement requirements to improve the productivity and effectiveness of GitLab and GitLab partner Professional Services professionals
- Develop assessment and certification program(s), including tiers, targeted personas, learning paths, objectives and supporting content
- Develop and maintain global ecosystem of certified training partners to help scale GitLab’s training delivery capabilities
- Collaborate with enablement colleagues to define cross-functional technology platform requirements to support current and future training and enablement delivery efforts

## Levels

## Senior Solutions Manager

The Senior Solutions Manager role extends the [Solutions Manager](#intermediate-requirements) role.

### Responsibilities
- 7+ years relevant experience relevant experience defining, developing, and executing customer services and/or training strategies, tactics and action plans
- Advanced experience and insight with the GitLab platform
- Demonstrated experience leading others and partnering with senior executives
- Advanced ability to think strategically about business, products, and technical challenges
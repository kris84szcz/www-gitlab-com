---
layout: job_family_page
title: "Operations Analysts"
---

# Operations Analyst Roles at GitLab

Operations Analysts at GitLab work on business operations and are focused on improving and enhancing business data management and analysis. They work in departments with directors and managers, and across other areas of the organization to liaison on business operations.

Unless otherwise specified, all Operations Analyst roles at GitLab share the following requirements and responsibilities. 

<a id="intermediate-requirements"></a>

#### Requirements

- Consistent track record of using quantitative analysis to impact key business decisions
- Proficiency in the English language, with excellent written and oral communication skills
- Positive and solution-oriented mindset
- Excellent analytical skills
- An inclination towards communication, inclusion, and visibility
- Self-motivated and self-managing, with strong organizational skills
- Ability to present data using detailed reports and charts
- Demonstrable strategic thinking skills
- Confidentiality in handling sensitive financial information
- Share our values, and work in accordance with those values
- Ability to thrive in a fully remote organization
- BS degree in Finance, Accounting or Economics

#### Nice-to-have's

- Experience in a peak performance organization
- Experience working with a remote team
- Experience working with a global or otherwise multicultural team

#### Responsibilities
 - Develop resource utilization and forecasting models to meet business and financial objectives
 - Develop financial models to provide data-driven guidance on cost decisions
 - Interface with various departments to provide timely, accurante and relevant data
 - Review business processes and policies to help enhance workflows 
 - Manage, analyze and report departmental KPIs

## Specialties

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab here.

### Infrastructure

The **Infrastructure Department** is the primary responsible party for the **availability**, **reliability**, **performance**, and **scalability** of all user-facing services (most notably **GitLab.com**). Other departments and teams contribute greatly to these attributes of our service as well. In these cases it is the responsibility of the Infrastructure Department to close the feedback loop with monitoring and metrics to drive accountability.

The Operations Analyst, Infrastructure is a key member of the Infrastructure team and works to enhance and improve our business operations and our forecasting and financial modeling capabilities, developing sound business practices within the Infrastructure Department to guide infrastructure resource utilization and associated costs optimizations over time, as well as tracking and modeling of all relevant KPIs.

#### Responsibilities

- Develop infrastructure resource utilization and forecasting models to meet business and financial objectives
- Develop infrastructure financial models to provide data-driven guidance on cost decisions
- Interface with Finance, Legal and vendors to manage Infrastructure-related contracts 
- Interface with Sales to understand sales pipeline and its effect on Infrastructure
- Develop and maintain Infrastructure-centric sales collateral as it relates to GitLab.com
- Manage service level framework (SLIs, SLOs, SLAs) and associated error budgets to meet business objectives for GitLab.com
- Review Infrastructure business processes and policies and help enhance workflows in support of GitLab.com and Infrastructure-provided services to the rest of the company
- Develop a deep understanding of the infrastructure vendor landscape to help Infrastructure leaders select, work and optimize infrastructure usage

